import csv

file = './snippet/mini_profiles.csv'
def find_blood_donors_1(filename):    
    with open (filename) as f:
        reader = csv.DictReader(f)   
        yield ({dict['name'],dict['mail']} for dict in reader if dict['blood_group'].lower()=='ab-' and dict['age'] == '60' and dict['name'].lower().startswith('victoria') and dict['mail'].lower().endswith('@gmail.com') )
        
        
def find_blood_donors_2(filename):
    with open (filename) as f:
        reader = csv.DictReader(f)
        age = range(30,50)
        yield ({dict['name'],dict['mail']} for dict in reader if int(dict['age']) in age and dict['blood_group'].lower()=='b+' and dict['sex'].lower() == 'm' and dict['name'].lower().endswith('norris') and dict['mail'].lower().endswith('@yahoo.com') )


print(find_blood_donors_2(file))
